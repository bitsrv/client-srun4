package client.srun4

import spock.lang.Specification

class Srun4ClientTest extends Specification {
    String url = 'https://10.0.0.51:8001/api/'
    String accessToken = '2w5r6y7udwae2239u89dsf89c923c2er'

    def "查询用户"() {
        setup:
        def client = new Srun4Client(url, accessToken)

        when:
        def r = client.userView('guohongchen')

        then:
        r['user_name'] == 'guohongchen'
    }
}
