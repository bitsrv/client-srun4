package client.srun4

import java.security.MessageDigest
import wslite.rest.RESTClient

class Srun4Client {

    def restClient
    def accessToken

    Srun4Client(String url, String accessToken) {
        restClient = new RESTClient(url)
        restClient.httpClient.connectTimeout = 5000
        restClient.httpClient.readTimeout = 10000
        restClient.httpClient.useCaches = false
        restClient.httpClient.followRedirects = false
        restClient.httpClient.sslTrustAllCerts = true

        this.accessToken = accessToken
    }

    /**
     * 查看用户信息
     *
     * @param user_name 用户名
     *
     * 返回用户信息 Map类型
     *  user_id             账号id
     *  user_name           账号
     *  user_real_name      账号别名
     *  group_id            用户组id
     *  user_available      账号状态（0 正常 1禁用 2停机保号 3暂停）
     *  region_id           所属区域组id
     *  user_allow_chgpass  是否允许修改密码
     *  user_create_time    账号创建时间
     *  user_update_time    账号更新时间
     *  user_expire_time    账号过期时间
     *  user_status         是否欠费
     *  balance             余额
     *  mgr_name_create     创建此用户的管理员
     *  mgr_name_update     最后操作的管理员
     *  user_start_time     账号开始时间
     *  user_stop_time      账号停用时间
     *  ......
     */
    def userView(String user_name) {
        def resp = restClient.get(
                path: '/v1/user/view',
                query: ["access_token": this.accessToken, "user_name": user_name]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]
        } else {
            return null
        }
    }

    /**
     * 增加用户
     *
     * @param user_name 用户名
     * @param user_real_name 真实姓名
     * @param user_password 密码
     * @param group_id 用户组id
     * @param products_id 产品id,多个产品中间用,隔开 比如 1,2,3,5
     * @param exts 扩展字段 其中key可为 cert_num phone email等
     * @param user_expire_time 过期时间 格式必须为yyyy-mm-dd hh:mm:ss
     */
    def userAdd(String user_name, String user_real_name, String user_password, int group_id, String products_id, Map exts = null, String user_expire_time = null) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "user_real_name": user_real_name,
                "user_password": user_password,
                "group_id": group_id,
                "products_id": products_id
        ]

        if(exts) {
            data.putAll(exts)
        }

        if(user_expire_time) {
            data.put("user_expire_time", user_expire_time)
        }

        def resp = restClient.post(path: '/v1/users') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 开启/暂停接口
     * @param user_name 用户名或id
     * @param user_available 用户状态 0正常 1禁用 2停机保号 3暂停
     */
    def userStatusControl(String user_name, int user_available) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "user_available": user_available
        ]

        def resp = restClient.post(path: '/v1/user/user-status-control') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 在线设备
     *
     * @param user_name 用户名
     *
     * 返回用户信息，数组类型，每个元素为Map类型
     *  rad_online_id   在线设备id, 用户以及设备下线会用到
     *  os_name         操作系统型号
     *  ip              ip地址
     *  bytes_in        入流量
     *  bytes_out       出流量
     *  add_time        上线时间
     */
    def onlineEquipment(String user_name) {
        def resp = restClient.get(
                path: '/v1/base/online-equipment',
                query: ["access_token": this.accessToken, "user_name": user_name]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]
        } else {
            return null
        }
    }

    /**
     * 设备下线
     *
     * @param user_name 用户名
     * @param drop_type 下线类型 bytes_in > 0 为radius下线 bytes_in = 0 为proxy下线
     * @param rad_online_id 在线设备id
     *
     */
    def onlineDrop(String user_name, String drop_type, int rad_online_id) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "drop_type": drop_type,
                "rad_online_id": rad_online_id
        ]

        def resp = restClient.post(path: '/v1/base/online-drop') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 用户已绑定的产品列表
     *
     * @param user_name 用户名
     *
     * 返回用户已绑定的产品列表, 数组类型, 其中每个元素为Map
     *  user_id 用户id
     *  products_id 产品id
     *  products_name 产品名称
     *  billing_name 结算策略
     *  control_name 控制策略
     *  condition 使用条件
     *  checkout_date 结算日期
     *  sum_bytes 使用流量
     *  sum_seconds 使用次数
     *  sum_times 使用时长
     *  package_name 套餐名称
     *  user_balance 产品余额
     */
    def userProduct(String user_name) {
        def resp = restClient.get(
                path: '/v1/package/users-packages',
                query: ["access_token": this.accessToken, "user_name": user_name]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]
        } else {
            return null
        }
    }

    /**
     * 产品续费接口
     *
     * @param user_name 用户名
     * @param product 产品ID
     * @param amount 产品续费金额
     *
     */
    def productRecharge(String user_name, int product, double amount) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "product": product,
                "amount": amount
        ]

        def resp = restClient.post(path: '/v1/product/product-recharge') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 订购产品
     *
     * @param user_name 用户名
     * @param product 产品id
     * @param amount 金额
     *
     */
    def productSubscribe(String user_name, int product, double amount = 0.0) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "product": product,
                "amount": amount
        ]

        def resp = restClient.post(path: '/v1/product/subscribe') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 查询产品详情
     *
     * @param productId 产品id
     *
     * 返回产品详情
     */
    def productDetail(Integer productId) {
        def resp = restClient.get(
                path: "/v1/product/view",
                query: ["access_token": this.accessToken, "products_id": productId]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]
        } else {
            return null
        }
    }

    /**
     * 查询电子钱包余额
     *
     * @param user_name 用户名
     *
     * 返回电子钱包余额
     */
    def walletBalance(String user_name) {
        def resp = restClient.get(
                path: '/v1/user/balance',
                query: ["access_token": this.accessToken, "user_name": user_name]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]["balance"]
        } else {
            return null
        }
    }

    /**
     * 电子钱包充值
     *
     * @param user_name 用户id
     * @param pay_num 充值金额
     * @param pay_type_id 充值方式
     * @param order_no 订单号
     */
    def walletRecharge(String user_name, double pay_num, int pay_type_id, String order_no = '') {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "pay_num": pay_num,
                "pay_type_id": pay_type_id,
                "order_no": order_no
        ]

        def resp = restClient.post(path: '/v1/financial/recharge-wallet') {
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 根据证件号查看用户
     *
     * @param cert_num 证件号码
     *
     * 返回用户的上网账号
     */
    def userSearch(String cert_num) {
        def resp = restClient.get(
                path: '/bit/user/search',
                query: ["access_token": this.accessToken, "cert_num": cert_num]
        ).json

        if(resp["code"] == 0) {
            return resp["data"]["user_name"]
        } else {
            return null
        }
    }

    /**
     * 校验用户合法性
     *
     * @param user_name 用户名
     * @param password 密码
     *
     */
    def userCheck(String user_name, String password) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "password": this.generateMD5(password)
        ]

        def resp = restClient.post(path: '/bit/user/validate-users'){
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 修改用户密码（super）
     *
     * @param user_name 用户名
     * @param new_password 新密码
     *
     */
    def superResetPassword(String user_name, String new_password) {
        def data = [
                "access_token": this.accessToken,
                "user_name": user_name,
                "new_password": new_password
        ]

        def resp = restClient.post(path: '/v1/user/super-reset-password'){
            type 'application/x-www-form-urlencoded'
            urlenc data
        }.json

        if(resp["code"] == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * 生成 MD5 32位
     * @param str
     *
     * 返回md5码
     */
    private String generateMD5(String str) {
        MessageDigest digest = MessageDigest.getInstance("MD5")
        digest.update(str.bytes)
        return new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0')
    }
}

